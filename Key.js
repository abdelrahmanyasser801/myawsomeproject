import React, {useState, useRef, useEffect, createRef} from 'react';
import {
  Image,
  StyleSheet,
  TouchableOpacity,
  Button,
  ScrollView,
  Platform,
  KeyboardAvoidingView,
  Pressable,
  Keyboard,
  Text,
  StatusBar,
} from 'react-native';
import {
  Colors,
  DebugInstructions,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import Div from '../StyledComponents/Div';
import Paragraph from '../StyledComponents/Paragraph';
import Input from '../StyledComponents/Input';
import Stbutton from '../StyledComponents/Stbutton';

class Key extends React.Component {
  constructor(props) {
    super(props);
    const count = 5;
    this.clockCall;
    (this.inputlength = 4),
      (this.state = {
        time: {},
        seconds: 5,
        texts: [],
        timer: count,
        resend: true,
        disable: false,
      });
    this.timer = 0;
    this.startTimer = this.startTimer.bind(this);
    this.countDown = this.countDown.bind(this);
    for (let i = 0; i < this.inputlength; i++) {
      this.state.texts.push('');
    }
  }

  secondsToTime(secs) {
    let divisor_for_minutes = secs % (60 * 60);
    let divisor_for_seconds = divisor_for_minutes % 60;
    let seconds = Math.ceil(divisor_for_seconds);

    let obj = {
      s: seconds,
    };
    return obj;
  }
  componentDidMount() {
    let timeLeftVar = this.secondsToTime(this.state.seconds);
    this.setState({time: timeLeftVar});
    //     this.keyboardDidShowlistner=Keyboard.addListener(
    //         'keyboardDidShow',
    //         this._keyboardDidshow,
    //     );
    //     this.keyboardDidHidelistner=Keyboard.addListener(
    //         'keyboardDidHide',
    //         this._keyboardDidHide,
    //     );
    //   }
    //   componentWillUnmount(){
    //     this.keyboardDidShowlistner.remove()
    //     this.keyboardDidHidelistner.remove()
    //   }
    //   _keyboardDidshow=()=>{
    //     scrollView.scrollTo({y:50, animated: true})
    //   }
    //   _keyboardDidHide=()=>{
    //     scrollView.scrollTo({y:0, animated: true})
  }
  startTimer() {
    if (this.timer == 0 && this.state.seconds > 0) {
      this.timer = setInterval(this.countDown, 1000);
    }
  }

  countDown() {
    // Remove one second, set state so a re-render happens.
    let seconds = this.state.seconds - 1;
    this.setState({
      time: this.secondsToTime(seconds),
      seconds: seconds,
      resend: false,
    });

    // Check if we're at zero.
    if (seconds == 0) {
      clearInterval(this.timer);
      this.setState({
        time: this.secondsToTime(seconds),
        seconds: seconds,
      });
    }
  }
  renderInputs() {
    return this.state.texts.map((text, index) => {
      return (
        <Div key={index} style={styles.cell}>
          <TouchableOpacity>
            <Input
              ref={ref => {
                this['myRef'.concat(index)] = ref;
              }}
              onChangeText={text => this.handleChange(text, index)}
              keyboardType="number-pad"
              returnKeyType="done"
              maxLength={1}
              value={this.state.texts[index]}
              style={styles.celltext}></Input>
          </TouchableOpacity>
        </Div>
      );
    });
  }

  handleChange(text, index) {
    let texts = [];

    for (var i = 0; i < this.inputlength; i++) {
      if (i == index) {
        texts.push(text);
      } else {
        texts.push(this.state.texts[i]);
      }
    }
    this.setState({texts});

    if (!this.state.texts[index].trim() && index < this.inputlength - 1) {
      this['myRef'.concat(index + 1)].focus();
      this.setState({disable: true});
    } else if (this.state.texts[index].trim() && index != 0) {
      this['myRef'.concat(index - 1)].focus();
    }
  }

  decrement() {
    // if (this.state.timer === 0) {
    //   // this.seSstate.resend = true;
    //   // this.state.timer = 0;
    //   this.setState({timer: 0,
    //     resend:true
    //   })
    //   clearInterval(this.clockCall);
    // } else {
    //   this.setState({timer : timer - 1});
    // }
  }
  onResend() {
    // if (this.state.resend) {
    //   this.setState({timer: this.count,
    //     resend:false
    //   })
    //   clearInterval(this.clockCall);
    //   this.clockCall = setInterval(() => {
    //     this.decrement().bind(this);
    //   }, 1000);
    // }
  }

  render() {
    return (
      <Div style={styles.container}>
        <StatusBar
          backgroundColor={Colors.lighter}
          color={'black'}
          barStyle="dark-content"></StatusBar>
        <KeyboardAvoidingView
          behavior={Platform.IOS ? 'padding' : ''}
          style={styles.Keyboadrcontainer}>
          <Div style={styles.backcontainer}>
            <Image
              source={require('../assets/back.png')}
            />
          </Div>
          <Div style={styles.titlecontainer}>
            <Paragraph style={styles.title}>
              Enter the code you've just received
            </Paragraph>
          </Div>
          <Div>
            <Div style={styles.inputcontainer}>{this.renderInputs()}</Div>

            <Div style={styles.bottom}>
              <TouchableOpacity onPress={this.startTimer}>
                <Div style={styles.resend}>
                  {/* <Paragraph
                    style={{
                      color: 'rgb(21,158,218)'
                    }}>
                    Resend code ({this.state.time.s})
                  </Paragraph> */}
                  <TouchableOpacity>
                    <Pressable style={styles.button} onPress={this.startTimer}>
                      <Paragraph
                        style={{
                          color: this.state.resend
                            ? 'rgb(21,158,218)'
                            : 'rgb(97,104,122)',
                          textDecorationLine: 'underline',
                        }}>
                        Resend code ({this.state.time.s})
                      </Paragraph>
                    </Pressable>
                  </TouchableOpacity>
                </Div>
              </TouchableOpacity>
            </Div>
            <Div style={styles.btnContainer}>
              <TouchableOpacity
                disabled={this.state.texts[this.inputlength - 1].length < 1}
                style={{
                  backgroundColor:
                    this.state.texts[this.inputlength - 1].length < 1
                      ? 'gray'
                      : 'rgb(21,158,218)',
                  paddingHorizontal: '40%',
                  // width: 292,
                  // height: 47,
                  borderRadius: 4,
                  alignSelf: 'center',
                }}>
                <Text style={styles.verifytext}>VERIFY</Text>
              </TouchableOpacity>
            </Div>
          </Div>
        </KeyboardAvoidingView>
      </Div>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 14,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  Keyboadrcontainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    //    padding: 10,
  },
  backcontainer: {
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    alignSelf: 'stretch',
    paddingHorizontal: 14,
    paddingTop: 10,
  },
  titlecontainer: {
    height: '8%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingHorizontal: 14,
    marginTop: 38,
  },
  title: {
    //marginVertical: 20,
    fontSize: 17,
    color: 'rgb(97,104,122)',
    // position: 'relative',
    // right: '5%',
    // textAlign: 'left',
  },
  inputcontainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 14,
  },
  cell: {
    //width: '22%',
    flex: 1,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1.5,
    borderBottomColor: 'rgb(227,227,227)',
  },
  celltext: {
    textAlign: 'center',
    fontSize: 15,
    color: 'rgb(21,158,218)',
  },

  bottom: {
    flex: 0.56,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingHorizontal: 14,

  },
  resend: {
    //width: 300,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    

  },
  resendText: {
    // color: 'rgb(21,158,218)',
    // alignItems: 'center',
    // justifyContent: 'center',
  },
  // verifyButton: {
  //   paddingHorizontal: 14,
  //   width: '100%',
  //   height: 47,
  //   borderRadius: 4,
  //   backgroundColor: 'rgb(21,158,218)',
  // },

  btnContainer: {
    flex: 0.29,
    marginTop: 12,
    justifyContent: 'flex-end',
  },
  verifytext: {
    textAlign: 'center',
    fontSize: 13,
    paddingVertical: 14,
    // color: 'white',
    color: 'white',
  },
});

export default Key;
